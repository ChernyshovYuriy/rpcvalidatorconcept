package sdk.rpc.commands;

import sdk.rpc.RPCMessage;
import sdk.validator.ValidatorVisitor;

import java.util.Vector;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/29/14
 * Time: 9:41 PM
 */
public class RegisterAppInterface extends RPCMessage {

    private String appName;

    private int appId;

    private Vector<String> vectorNames;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public Vector<String> getVectorNames() {
        return vectorNames;
    }

    public void setVectorNames(Vector<String> vectorNames) {
        this.vectorNames = vectorNames;
    }

    @Override
    public boolean validate(ValidatorVisitor validatorVisitor) {
        return validatorVisitor.visit(this);
    }
}