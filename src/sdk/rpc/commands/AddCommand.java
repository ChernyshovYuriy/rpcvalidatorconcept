package sdk.rpc.commands;

import sdk.rpc.RPCMessage;
import sdk.validator.ValidatorVisitor;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/29/14
 * Time: 9:42 PM
 */
public class AddCommand extends RPCMessage {

    @Override
    public boolean validate(ValidatorVisitor validatorVisitor) {
        return validatorVisitor.visit(this);
    }
}