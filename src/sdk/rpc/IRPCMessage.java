package sdk.rpc;

import sdk.validator.ValidatorVisitor;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/29/14
 * Time: 9:39 PM
 */
public interface IRPCMessage {

    public boolean validate(ValidatorVisitor validatorVisitor);
}