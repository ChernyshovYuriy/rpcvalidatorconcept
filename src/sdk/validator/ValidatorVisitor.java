package sdk.validator;

import sdk.rpc.commands.AddCommand;
import sdk.rpc.commands.RegisterAppInterface;
import sdk.rpc.commands.UnregisterAppInterface;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/29/14
 * Time: 9:38 PM
 */
public interface ValidatorVisitor {

    public boolean visit(RegisterAppInterface registerAppInterface);
    public boolean visit(UnregisterAppInterface unregisterAppInterface);
    public boolean visit(AddCommand addCommand);
}