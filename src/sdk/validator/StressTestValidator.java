package sdk.validator;

import sdk.rpc.commands.AddCommand;
import sdk.rpc.commands.RegisterAppInterface;
import sdk.rpc.commands.UnregisterAppInterface;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/29/14
 * Time: 9:50 PM
 */
public class StressTestValidator implements ValidatorVisitor {

    @Override
    public boolean visit(RegisterAppInterface registerAppInterface) {
        return true;
    }

    @Override
    public boolean visit(UnregisterAppInterface unregisterAppInterface) {
        return true;
    }

    @Override
    public boolean visit(AddCommand addCommand) {
        return true;
    }
}