package sdk.validator;

import sdk.rpc.commands.AddCommand;
import sdk.rpc.commands.RegisterAppInterface;
import sdk.rpc.commands.UnregisterAppInterface;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/29/14
 * Time: 9:49 PM
 */
public class DefaultValidator implements ValidatorVisitor {

    private static final int RAI_NAME_MAX_LENGTH = 100;
    private static final int RAI_ID_MAX_VALUE = 100;
    private static final int RAI_VECTOR_MAX_SIZE = 10;
    private static final int RAI_VECTOR_MIN_SIZE = 0;

    @Override
    public boolean visit(RegisterAppInterface registerAppInterface) {
        return registerAppInterface != null &&
                registerAppInterface.getAppId() <= RAI_ID_MAX_VALUE &&
                registerAppInterface.getAppName() != null &&
                registerAppInterface.getAppName().length() <= RAI_NAME_MAX_LENGTH &&
                registerAppInterface.getVectorNames() != null &&
                registerAppInterface.getVectorNames().size() <= RAI_VECTOR_MAX_SIZE;
    }

    @Override
    public boolean visit(UnregisterAppInterface unregisterAppInterface) {
        return true;
    }

    @Override
    public boolean visit(AddCommand addCommand) {
        return true;
    }
}