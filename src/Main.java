import sdk.rpc.commands.RegisterAppInterface;
import sdk.validator.DefaultValidator;
import sdk.validator.StressTestValidator;
import sdk.validator.ValidatorVisitor;

import java.util.Vector;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/29/14
 * Time: 9:55 PM
 */
public class Main {

    public static void main(String[] args) {
        new Main();
    }

    public Main() {

        // Create RegisterAppInterface
        RegisterAppInterface registerAppInterface = new RegisterAppInterface();

        // Valid parameters
        System.out.println("Valid parameters");
        registerAppInterface.setAppId(15);
        registerAppInterface.setAppName("App Name");
        registerAppInterface.setVectorNames(new Vector<String>());

        // Validate
        ValidatorVisitor validatorVisitor = new DefaultValidator();
        boolean result = registerAppInterface.validate(validatorVisitor);

        System.out.println("Validate default: " + result);

        // Invalid parameters
        System.out.println("Invalid parameters");
        registerAppInterface.setAppId(555);
        registerAppInterface.setAppName("AppName123AppName123AppName123AppName123AppName123AppName123AppName123" +
                "AppName123AppName123AppName123AppName123AppName123AppName123AppName123AppName123AppName123AppName123");
        registerAppInterface.setVectorNames(null);

        result = registerAppInterface.validate(validatorVisitor);
        System.out.println("Validate default: " + result);

        // Validate
        validatorVisitor = new StressTestValidator();
        result = registerAppInterface.validate(validatorVisitor);

        System.out.println("Validate stress: " + result);
    }
}